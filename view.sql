-- View para relacionar Empresa e quantidade de acionistas
CREATE VIEW Empresa_Acionistas AS
SELECT e.nome AS Empresa, COUNT(ca.id) AS Qtd_Acionistas
FROM empresa e 
INNER JOIN acao a ON e.id = a.empresa_id 
INNER JOIN conta_acao ca ON a.id = ca.acao_id
GROUP BY e.nome
ORDER BY e.nome;

-- View para relacionar Empresa e média do seu rendimento anual
CREATE VIEW Empresa_Rendimento AS 
SELECT e.nome AS Empresa, ROUND(AVG(r.percentual), 2) AS Media_Rendimento_Anual
FROM empresa e 
INNER JOIN rendimento r ON e.id = r.empresa_id
GROUP BY e.nome
ORDER BY e.nome;