insert into tipo_conta (id, tipo) values (1, 'Deposito');
insert into tipo_conta (id, tipo) values (2, 'Pagamento');
insert into tipo_conta (id, tipo) values (3, 'Corrente');
insert into tipo_conta (id, tipo) values (4, 'Poupança');
insert into tipo_conta (id, tipo) values (5, 'Salario');
insert into tipo_conta (id, tipo) values (6, 'Universitária');
insert into tipo_conta (id, tipo) values (7, 'Internet Banking');
insert into tipo_conta (id, tipo) values (8, 'Digital');
insert into tipo_conta (id, tipo) values (9, 'Menor de Idade');

insert into tipo_acao (id, tipo, descricao) values (1, 'Ordinaria', 'Como falamos, entre os tipos de ações que existem, esta é uma ação que garante a quem a possui o direito a voto e à participação nas decisões da companhia. Vale lembrar que a diferença que essa participação faz na administração da empresa depende da quantidade de ações ordinárias que um investidor possui.');
insert into tipo_acao (id, tipo, descricao) values (2, 'Preferencial', 'As ações preferenciais, como você viu, dão preferência - daí o nome - no recebimento de valores como distribuição de lucros. Ou seja, um acionista preferencial receberá dividendos e outras compensações, como Juros sobre Capital Próprio, primeiro do que quem possui ações ordinárias da mesma empresa.');
insert into tipo_acao (id, tipo, descricao) values (3, 'Normativa', 'Até 1990, podiam existir ações que não eram nominativas, ou seja, não eram vinculadas ao nome e CPF do investidor. A partir dessa data, todas as ações passaram a ser nominativas, com o intuito de oferecer mais segurança e transparência a quem investe. Além de ficar tudo registrado na Bolsa de Valores do Brasil, a B3, a identificação também é realizada no Livro de Registro de Ações Nominativas da empresa. Portanto, daquela época em diante, todas as ações compradas por uma pessoa estão em seu nome. Além da segurança, essa mudança também buscou evitar fraudes e sonegação de impostos.');
insert into tipo_acao (id, tipo, descricao) values (4, 'Escriturais', 'Nos EUA, as ações escriturais são chamadas de book shares. Esse tipo de ação pode ser autorizada pelo estatuto da empresa e mantido em contas de depósito, em nome do investidor que a possui sem a necessidade de emissão de certificado, na instituição que escolher (desde que seja autorizada a fazer isso pela CVM). Por não possuir um documento físico, ao negociar uma ação escritural também não há movimentação de documentos. Dessa forma, o crédito e/ou o débito dos valores relativos à negociação são feitos diretamente na conta de quem comprou ou vendeu.');
insert into tipo_acao (id, tipo, descricao) values (5, 'Units', 'O certificado de depósito de ações (Unit) é uma espécie de pacote composto por ativos diferentes. Ou seja, uma única Unit pode conter diferentes tipos de ações, como ações ON e PN. Um exemplo de Units são as ações do Banco Inter BIDI11, que possuem 1 ação BIDI3 e 2 ações BIDI4.');
insert into tipo_acao (id, tipo, descricao) values (6, 'Blue Chip', 'Um dos tipos de ações mais buscados por investidores na Bolsa de valores são as blue chips. Essa classificação não é oficial, mas é bastante usada para caracterizar ações com grande negociação na Bolsa e, consequentemente, maior valor de mercado. Por serem muito valorizadas por quem investe, essas ações têm alta liquidez, isto é, são mais fáceis de vender, pois não é tão difícil encontrar alguém com interesse em comprá-las.');
insert into tipo_acao (id, tipo, descricao) values (7, 'Mid Caps', 'Se as blue chips estão no pódio entre as mais concorridas do mercado, podemos entender que as mid caps são tipos de ações intermediárias. Isso significa que algumas podem oferecer maior liquidez, mas outras nem tanto. Por oferecem muitas vezes potencial de valorização maior até que as blue chips, elas chamam a atenção de alguns investidores. No entanto, é preciso ir com cuidado, pois nem todas as mid caps oferecem essa oportunidade.');
insert into tipo_acao (id, tipo, descricao) values (8, 'Small Caps', 'No terceiro degrau da escada, atrás das mid caps e das blue chips, vamos encontrar as ações chamadas de Small Caps. Elas são ações de empresas com menor capitalização. No entanto, isso nem sempre significa algo ruim, já que por serem mais baratas, esse tipo de ação podem gerar uma rentabilidade maior. Porém, assim como falamos sobre as mid caps, nem sempre comprar uma ação muito barata é sinônimo de bons resultados.');

insert into tipo_cliente (id, tipo) values (1, 'Titular');
insert into tipo_cliente (id, tipo) values (2, 'Cotitular');
insert into tipo_cliente (id, tipo) values (3, 'Dependente');

insert into taxa (id, tipo, taxa) values (1, 'Cartão', 0.04);
insert into taxa (id, tipo, taxa) values (2, 'Bancária', 0.09);
insert into taxa (id, tipo, taxa) values (3, 'Documento de Ordem de Crédito', 0.05);
insert into taxa (id, tipo, taxa) values (4, 'Transferência Eletrônica Disponivel', 0.1);
insert into taxa (id, tipo, taxa) values (5, 'Boleto', 0.04);
insert into taxa (id, tipo, taxa) values (6, 'Voucher', 0.03);
insert into taxa (id, tipo, taxa) values (7, 'Duplicata', 0.08);

insert into empresa (id, nome, descricao, cnpj, telefone, email) values (1, 'Schaden LLC', 'Empresa de Vídeo Games', '82.255.285/0001-25', '450-638-2082', 'smelvin0@sourceforge.net');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (2, 'Treutel Inc', 'Empresa de Eletrodomésticos', '86.247.255/0201-26', '226-701-5950', 'mcoltart1@webeden.co.uk');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (3, 'Smitham-Carter', 'Empresa de Bonecas', '83.227.285/0031-27', '929-270-2619', 'meslinger2@businessweek.com');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (4, 'Bahringer and Sons', 'Empresa de Instrumentos Musicais', '83.227.285/0001-28', '652-240-3779', 'jwain3@technorati.com');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (5, 'Trantow-Beer', 'Empresa de Eletrônicos', '83.227.285/0001-29', '482-327-6933', 'jklisch4@sciencedaily.com');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (6, 'Johnson, Haley and Bosco', 'Empresa de Roupas', '83.227.285/0001-30', '291-323-1869', 'lion5@answers.com');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (7, 'Nolan-Powlowski', 'Empresa de Jóias', '83.227.285/0001-31', '484-372-7860', 'wjopling6@house.gov');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (8, 'Gaylord, Swaniawski and Bogan', 'Empresa de Bonés', '83.227.285/0001-32', '781-867-6612', 'bfermoy7@illinois.edu');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (9, 'Bogan and Sons', 'Empresa de Vestidos', '83.227.285/0001-33', '702-253-0730', 'fflatt8@telegraph.co.uk');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (10, 'Schmeler and Sons', 'Empresa de Tênis', '83.227.285/0001-34', '702-376-2205', 'cfitch9@reuters.com');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (11, 'Monahan-Pagac', 'Empresa de Produtos Químicos', '83.227.285/0001-35', '172-212-9813', 'huridgea@toplist.cz');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (12, 'Dicki Inc', 'Empresa de Farmacos', '83.227.285/0001-36', '139-536-2779', 'jdonavanb@ow.ly');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (13, 'Parisian, Bailey and McCullough', 'Empresa Alimentícia', '83.227.285/0001-37', '412-795-5616', 'sperseyc@soundcloud.com');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (14, 'Howe Inc', 'Empresa Metalúrgica', '83.227.285/0001-38', '360-863-4174', 'gtomczykowskid@engadget.com');
insert into empresa (id, nome, descricao, cnpj, telefone, email) values (15, 'Schaden, Stamm and Yundt', 'Empresa Mineradora', '83.227.285/0001-39', '814-574-7684', 'bhavicke@amazon.com');

insert into corretora (id, nome, descricao, cnpj, telefone, email) values (1, 'Schumm-Stehr', 'É uma corretora fundada em 2010 e que em poucos anos se tornou uma das principais fintechs de investimento do mundo. Além de ser uma empresa do Santander, ela possui todas as autorizações do Banco Central e da CVM, o que garante a confiabilidade da instituição.', '42.754.321/3215-38', '986-645-9336', 'rrewan0@blogs.com');
insert into corretora (id, nome, descricao, cnpj, telefone, email) values (2, 'Aufderhar-Klocko', 'É uma corretora de valores bastante conhecida no Brasil e que transparece credibilidade. Essa plataforma foi criada em 2011 para simplificar o acesso ao mundo dos investimentos, oferecendo um aplicativo leve e intuitivo para seus clientes. Um ponto interessante é que, durante o pregão da bolsa de valores, ela transmite programas ao vivo para ajudar na tomada de decisão de investimentos. Em paralelo, são disponibilizados centenas de vídeos educacionais.', '43.546.513/3321-39', '124-158-1771', 'hjanaud1@tripadvisor.com');
insert into corretora (id, nome, descricao, cnpj, telefone, email) values (3, 'Lemke Group', 'É uma das corretoras mais conceituadas do Brasil. Atualmente, ela possui mais de 3 milhões de clientes, com mais de R$ 715 bilhões sob custódia. Seu maior diferencial é a assessoria personalizada que é oferecida. Independentemente da cidade em que se encontra, você tem o acompanhamento de um profissional. A propósito, essa orientação especializada ajuda a escolher os ativos mais próximos do seu perfil de investimento. Para quem tem mais de R$ 5 mil investidos na plataforma, a corretora disponibiliza um cartão de crédito com cashback e sem custo adicional.', '45.732.831/0322-40', '146-142-8795', 'ahiscoke2@samsung.com');
insert into corretora (id, nome, descricao, cnpj, telefone, email) values (4, 'Klocko-Howell', 'É uma corretora de valores que costuma ser indicada para pequenos investidores e pessoas com pouco conhecimento sobre o mercado financeiro. Um dos seus diferenciais é oferecer um amplo conteúdo educacional para os clientes. Para além disso, é possível encontrar diversos ativos financeiros, como CDBs, debêntures, LCIs, LCAs, CRIs, CRAs, LHs, LIs e fundos de renda fixa.', '46.766.841/0541-41', '281-292-8352', 'dmeese3@wikia.com');
insert into corretora (id, nome, descricao, cnpj, telefone, email) values (5, 'Thompson, Fahey and Fay', 'Para quem já tem um conhecimento um pouco mais avançado no mercado e quer operar em day trade ou swing trade, a Clear é uma excelente alternativa. Afinal, ela oferece ferramentas de negociação inovadoras com os melhores preços. Só para ilustrar, a corretora tem mais de 1 milhão de clientes ativos e mais de R$ 300 bilhões sob custódia. Entretanto, como não oferece títulos de renda fixa, nem títulos do Tesouro Direto, ela é mais indicada para investidores com perfis arrojados. Para quem costuma operar no day trade, por exemplo a plataforma oferece diversas ferramentas, como: MetaTrader5; ProfitPlus; FlashTrader; Tryd Pro; SmarttBot; FlashChart. Um dos pontos interessantes da Clear é que, além dos benefícios, ela não cobra taxas, a não ser que você use o sistema de Zeragem Compulsória. Em outras palavras, isso significa que é o fechamento automático das posições quando o investidor atinge o limite máximo de risco permitido.', '47.743.841/0321-42', '685-678-5617', 'tmcsarry4@csmonitor.com');

insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (1, 'Temp', 11, 4, 1, 55.3);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (2, 'Ronstring', 4, 1, 7, 371.75);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (3, 'Flowdesk', 6, 1, 2, 446.28);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (4, 'Zoolab', 10, 4, 3, 475.69);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (5, 'Fixflex', 2, 3, 4, 161.61);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (6, 'Zamit', 3, 5, 7, 483.15);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (7, 'It', 3, 5, 3, 924.7);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (8, 'Matsoft', 13, 4, 3, 268.2);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (9, 'Job', 15, 1, 4, 282.95);
insert into acao (id, nome, empresa_id, corretora_id, tipo_acao_id, valor_base) values (10, 'Zathin', 3, 5, 5, 260.75);

insert into bandeira (id, nome) values (1, 'Visa');
insert into bandeira (id, nome) values (2, 'Mastercard');
insert into bandeira (id, nome) values (3, 'Elo');
insert into bandeira (id, nome) values (4, 'American Express');
insert into bandeira (id, nome) values (5, 'Hipercard');

insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (1, 'Seguro de vida', 'Morte acidental ou natural; Invalidez permanente total ou parcial em caso de acidente; Auxílio funeral; Doenças graves ou terminais; Determinados gastos médicos; Perda de autonomia em caso de doenças que impedem as atividades essenciais do dia a dia.', 7998.24, 18058);
insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (2, 'Seguro de acidentes pessoais', 'Morte acidental; Invalidez permanente total ou parcial causada por acidente; Gastos médicos e internação hospitalar devido a acidente; Auxílio funeral.', 32166.93, 8735);
insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (3, 'Seguro saúde', 'Consultas e exames; Serviços ambulatoriais; Atendimento emergencial; Pré-natal e obstetrícia; Terapias; Internações; Procedimentos cirúrgicos.', 22051.5, 13333);
insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (4, 'Seguro de veículos', 'Acidentes por colisão ou choque; Perda total ou parcial do veículo por explosão, incêndio, alagamento, etc.; Roubo ou furto do veículo ou partes dele; Danos materiais causados por terceiros; Danos causados a terceiros.', 5551.9, 5511);
insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (5, 'Seguro residencial', 'Furto ou roubo de bens na residência; Danos causados por fenômenos climáticos e naturais como granizo, raios, deslizamentos, terremotos, ciclones, vendavais, etc.; Danos estruturais e em sistemas elétrico e hidráulico; Incêndio, fumaça, alagamento, explosão, etc.; Quebra de vidros em geral; Danos a terceiros provocados pelo segurado e outros residentes; Pagamento do aluguel durante o período de reparos ou mudança em caso de estragos na residência.', 8419.46, 7020);
insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (6, 'Seguro viagem', 'Acidentes pessoais durante a viagem; Invalidez permanente por acidente; Assistência médica e odontológica; Medicamentos; Extravio ou atraso de bagagem; Imprevistos de viagem como atraso e cancelamento de voo, problemas na reserva de hotéis, etc.; Hospedagem e alimentação; Traslados.', 10493.82, 9699);
insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (7, 'Seguro de equipamentos', 'Roubo ou furto do equipamento; Quebra acidental; Oxidação do aparelho; Danos elétricos durante a carga; Derramamento de líquidos.', 18025.67, 4986);
insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (8, 'Seguro de empréstimo', 'Morte acidental ou natural; Invalidez permanente total ou parcial por acidente; Demissão sem justa causa; Perda de renda (autônomo).', 6287.59, 19793);
insert into seguro (id, tipo, descricao, mensalidade, indenizacao) values (9, 'Seguro de cartão de crédito', 'Furto e perda de cartão de crédito; Roubo após saque; Saque e compras sob coação de criminosos; Compras com cartão roubado; Roubo de bolsa ou carteira; Desemprego ou perda de renda; Morte ou invalidez por crime.', 10491.46, 2218);

--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=

insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (1, '011', 1760, 1760, 'OCrsyStp', 'cHgeEWzLeu3', '07/09/2022', 5);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (2, '007', 3829, 3829, 'Ezi1nU4', 'QHDaAioz', '08/03/2022', 4);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (3, '011', 1871, 685, 'LitRbyd', 'hvWPAEATCvd', '11/04/2021', 8);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (4, '002', 1066, 1066, 'Kes64fV7y', 'hokpVNrF', '17/05/2021', 8);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (5, '006', 4685, 1212, '1CFMPRLGADM', 'YtnmqUMyFOWe', '29/08/2021', 2);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (6, '012', 2493, 1039, 'iduFHBps2', 'UVu7998BM0', '22/09/2022', 1);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (7, '013', 563, 563, 'xnkDVu8HKcy', 'u4BG2k', '07/08/2022', 9);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (8, '006', 1961, 1124, 'uTR27Z2QcM4', 'eOTb4RV6m1u', '16/01/2022', 3);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (9, '004', 3629, 2958, 'KPsJn70G', 'P5yV9CKV', '19/12/2022', 4);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (10, '001', 1169, 1169, 'U5qq0tV', 'fHaU4LDKEDGL', '06/11/2021', 5);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (11, '009', 2830, 2830, 'IkvT1oZwFNxO', 'CXEipi9', '03/09/2022', 5);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (12, '010', 3150, 1027, 'CCftlmlUNPGm', 'jEMR1HUGYvu', '13/03/2022', 4);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (13, '007', 3465, 3465, 'XEBPJq7', 'pozkdXj0', '05/03/2022', 9);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (14, '010', 4493, 4208, 'vvTiBw0Je', 'LmSkVQzQV9bc', '15/09/2021', 9);
insert into conta (id, numero_banco, limite, saldo, senha, salt, data_cadastro, tipo_conta_id) values (15, '001', 2535, 2535, '7FbFTG8Wwi', '2uwAL3Jd', '07/10/2022', 4);

insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (1, 13, 81551.64, 0.0, '09/03/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (2, 5, 53873.43, 0.53, '03/06/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (3, 4, 44028.59, 0.24, '22/05/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (4, 13, 10864.83, 0.52, '02/04/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (5, 3, 20339.92, 0.48, '19/08/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (6, 5, 91199.46, 0.66, '20/06/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (7, 9, 33697.83, 0.26, '06/08/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (8, 7, 72017.78, 0.8, '01/03/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (9, 7, 22215.54, 0.16, '11/06/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (10, 12, 39720.03, 0.34, '06/11/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (11, 3, 16092.37, 0.89, '10/04/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (12, 15, 28244.93, 0.01, '10/05/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (13, 13, 99067.31, 0.35, '21/06/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (14, 13, 71203.6, 0.81, '25/11/2022');
insert into emprestimo (id, conta_id, valor_total, juros, data_realizacao) values (15, 4, 17405.72, 0.54, '28/07/2022');

insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (1, 1, 20000.00, '19/05/2022', '24/07/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (2, 2, 24500.00, '02/07/2022', '04/12/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (3, 3, 23210.00, '30/05/2022', '14/06/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (4, 4, 5032.00, '25/05/2022', '24/06/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (5, 5, 18420.00, '17/11/2022', '26/12/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (6, 6, 38490.00, '27/07/2022', '07/08/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (7, 7, 23540.00, '20/09/2022', '24/11/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (8, 8, 55103.0, '20/10/2022', '08/11/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (9, 9, 9534.0, '05/07/2022', '29/10/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (10, 10, 20321.0, '15/11/2022', '23/12/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (11, 11, 7589.00, '21/05/2022', '08/09/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (12, 12, 23213.00, '22/06/2022', '05/09/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (13, 14, 53134.00, '28/07/2022', '31/07/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (14, 15, 11195.00, '27/11/2022', '24/12/2022');
insert into parcela (id, emprestimo_id, valor, data_pagamento, data_agendada) values (15, 1, 16032.00, '17/10/2022', '12/12/2021');

insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (1, 5, 2, '3587481162678756', 359, '23/10/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (2, 12, 3, '201603705242079', 114, '07/07/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (3, 12, 3, '5602254576802163', 563, '28/03/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (4, 11, 4, '56022434316289351', 915, '10/11/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (5, 15, 5, '67098288783717062', 136, '25/11/2021');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (6, 14, 1, '3572421724159959', 326, '01/08/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (7, 11, 2, '30039632110641', 513, '04/05/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (8, 12, 3, '0604757762092341311', 761, '31/05/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (9, 6, 2, '3540030656432844', 401, '13/10/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (10, 11, 4, '3562879922547428', 307, '24/03/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (11, 9, 4, '3581829787788085', 731, '26/12/2021');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (12, 10, 2, '3587505721200841', 978, '23/06/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (13, 14, 5, '201460229247021', 850, '23/09/2022');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (14, 10, 3, '374283182958025', 438, '18/11/2021');
insert into cartao (id, conta_id, bandeira_id, numero, cvv, data_validade) values (15, 8, 1, '5602211622620643', 823, '03/04/2022');

insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (1, 'Marven Durden', '884.423.702-42', 'mdurden0@a8.net', '932-972-5235', '06/07/1992');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (2, 'Menard Kahler', '510.158.099-60', 'mkahler1@chronoengine.com', '138-178-4351', '18/06/1971');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (3, 'Claus Gosney', '695.360.038-13', 'cgosney2@joomla.org', '777-272-3303', '25/12/1991');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (4, 'Arvy Gresly', '364.284.814-13', 'agresly3@dyndns.org', '149-504-8914', '14/01/1978');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (5, 'Viviana Mumbey', '512.779.946-41', 'vmumbey4@vk.com', '215-973-2473', '10/02/1983');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (6, 'Sydney McElrath', '840.673.257-11', 'smcelrath5@list-manage.com', '231-224-1751', '31/08/1976');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (7, 'Saraann Filmer', '331.622.707-58', 'sfilmer6@github.com', '272-938-0950', '19/10/2003');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (8, 'Clementius Leckey', '570.117.073-01', 'cleckey7@mayoclinic.com', '607-272-9019', '21/04/1987');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (9, 'Alphonse Roylance', '534.946.504-03', 'aroylance8@samsung.com', '644-389-1469', '02/10/1971');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (10, 'Maitilde McPeck', '860.218.333-09', 'mmcpeck9@twitpic.com', '657-357-2400', '20/10/1999');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (11, 'Mauricio Megroff', '585.336.371-95', 'mmegroffa@last.fm', '920-823-6148', '14/01/1999');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (12, 'Dilly O''Carrol', '422.109.819-80', 'docarrolb@about.com', '687-233-4563', '23/11/1997');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (13, 'Xenos Gillebert', '958.071.577-70', 'xgillebertc@moonfruit.com', '509-703-5069', '22/04/1980');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (14, 'Suellen Arbuckel', '271.720.065-77', 'sarbuckeld@usgs.gov', '278-200-3989', '09/09/1983');
insert into cliente (id, nome, cpf, email, telefone, data_nascimento) values (15, 'Luci Southcombe', '888.031.496-35', 'lsouthcombee@sina.com.cn', '157-208-1712', '19/09/1985');

--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=--=-=-=-=--=-=-=-=-=

insert into seguro_conta (conta_id, seguro_id, data_validade) values (10, 4, '08/02/2022');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (14, 3, '26/11/2022');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (2, 6, '31/12/2021');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (5, 4, '21/11/2021');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (5, 1, '17/08/2022');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (4, 2, '29/04/2022');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (12, 1, '06/10/2022');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (15, 7, '12/08/2022');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (14, 2, '06/03/2022');
insert into seguro_conta (conta_id, seguro_id, data_validade) values (11, 5, '02/01/2022');

insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (1, 1, 2);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (2, 2, 3);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (3, 3, 3);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (4, 4, 2);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (5, 5, 3);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (6, 6, 3);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (7, 7, 2);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (8, 8, 1);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (9, 9, 1);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (10, 10, 2);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (11, 11, 2);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (12, 12, 1);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (13, 13, 2);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (14, 14, 3);
insert into conta_cliente (cliente_id, conta_id, tipo_cliente_id) values (15, 15, 2);

insert into empresa_cliente (cliente_id, empresa_id, data_assinatura, data_validade, contrato) values (1, 1, '14/01/2021', '28/12/2023', '/media/doc/contrato/contrato1.pdf');
insert into empresa_cliente (cliente_id, empresa_id, data_assinatura, data_validade, contrato) values (2, 2, '19/04/2021', '13/01/2023', '/media/doc/contrato/contrato2.pdf');
insert into empresa_cliente (cliente_id, empresa_id, data_assinatura, data_validade, contrato) values (3, 3, '25/08/2021', '17/01/2023', '/media/doc/contrato/contrato3.pdf');
insert into empresa_cliente (cliente_id, empresa_id, data_assinatura, data_validade, contrato) values (4, 4, '18/06/2021', '28/03/2023', '/media/doc/contrato/contrato4.pdf');
insert into empresa_cliente (cliente_id, empresa_id, data_assinatura, data_validade, contrato) values (5, 5, '16/03/2021', '27/08/2023', '/media/doc/contrato/contrato5.pdf');

insert into rendimento (id, mes, percentual, empresa_id) values (1, 'Janeiro/2022', 0.92, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (2, 'Janeiro/2022', 0.69, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (3, 'Janeiro/2022', 0.67, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (4, 'Janeiro/2022', 0.82, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (5, 'Janeiro/2022', 0.27, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (6, 'Fevereiro/2022', 0.11, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (7, 'Fevereiro/2022', 0.72, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (8, 'Fevereiro/2022', 0.41, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (9, 'Fevereiro/2022', 0.55, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (10, 'Fevereiro/2022', 0.97, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (11, 'Março/2022', 0.24, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (12, 'Março/2022', 0.75, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (13, 'Março/2022', 0.76, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (14, 'Março/2022', 0.78, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (15, 'Março/2022', 0.2, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (16, 'Abril/2022', 0.98, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (17, 'Abril/2022', 0.86, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (18, 'Abril/2022', 0.0, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (19, 'Abril/2022', 0.65, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (20, 'Abril/2022', 0.28, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (21, 'Maio/2022', 0.2, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (22, 'Maio/2022', 0.03, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (23, 'Maio/2022', 0.03, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (24, 'Maio/2022', 0.24, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (25, 'Maio/2022', 0.56, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (26, 'Junho/2022', 0.67, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (27, 'Junho/2022', 0.59, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (28, 'Junho/2022', 0.17, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (29, 'Junho/2022', 0.45, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (30, 'Junho/2022', 0.9, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (31, 'Julho/2022', 0.34, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (32, 'Julho/2022', 0.48, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (33, 'Julho/2022', 0.57, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (34, 'Julho/2022', 0.58, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (35, 'Julho/2022', 0.48, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (36, 'Agosto/2022',0.53, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (37, 'Agosto/2022',0.9, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (38, 'Agosto/2022',0.39, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (39, 'Agosto/2022',0.26, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (40, 'Agosto/2022',0.81, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (41, 'Setembro/2022', 0.16, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (42, 'Setembro/2022', 0.49, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (43, 'Setembro/2022', 0.63, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (44, 'Setembro/2022', 0.65, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (45, 'Setembro/2022', 0.96, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (46, 'Outubro/2022', 0.29, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (47, 'Outubro/2022', 0.6, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (48, 'Outubro/2022', 0.33, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (49, 'Outubro/2022', 0.16, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (50, 'Outubro/2022', 0.21, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (51, 'Novembro/2022', 0.97, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (52, 'Novembro/2022', 0.91, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (53, 'Novembro/2022', 0.36, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (54, 'Novembro/2022', 0.4, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (55, 'Novembro/2022', 0.01, 5);

insert into rendimento (id, mes, percentual, empresa_id) values (56, 'Dezembro/2022', 0.94, 1);
insert into rendimento (id, mes, percentual, empresa_id) values (57, 'Dezembro/2022', 0.11, 2);
insert into rendimento (id, mes, percentual, empresa_id) values (58, 'Dezembro/2022', 0.46, 3);
insert into rendimento (id, mes, percentual, empresa_id) values (59, 'Dezembro/2022', 0.52, 4);
insert into rendimento (id, mes, percentual, empresa_id) values (60, 'Dezembro/2022', 0.94, 5);

insert into deposito (id, conta_id, valor, data_realizacao) values (1, 10, 186, '03/01/2023');
insert into deposito (id, conta_id, valor, data_realizacao) values (2, 6, 1476, '15/11/2022');
insert into deposito (id, conta_id, valor, data_realizacao) values (3, 10, 2921, '22/05/2022');
insert into deposito (id, conta_id, valor, data_realizacao) values (4, 1, 1640, '06/01/2023');
insert into deposito (id, conta_id, valor, data_realizacao) values (5, 8, 1788, '25/02/2023');
insert into deposito (id, conta_id, valor, data_realizacao) values (6, 5, 86, '21/09/2021');
insert into deposito (id, conta_id, valor, data_realizacao) values (7, 3, 3388, '21/10/2021');
insert into deposito (id, conta_id, valor, data_realizacao) values (8, 15, 2016, '11/11/2022');
insert into deposito (id, conta_id, valor, data_realizacao) values (9, 13, 3874, '06/06/2022');
insert into deposito (id, conta_id, valor, data_realizacao) values (10, 15, 1164, '12/06/2023');

insert into saque (id, conta_id, valor, data_realizacao) values (1, 3, 3394, '13/07/2021');
insert into saque (id, conta_id, valor, data_realizacao) values (2, 8, 3742, '22/09/2022');
insert into saque (id, conta_id, valor, data_realizacao) values (3, 2, 2383, '10/09/2022');
insert into saque (id, conta_id, valor, data_realizacao) values (4, 13, 2645, '29/11/2022');
insert into saque (id, conta_id, valor, data_realizacao) values (5, 13, 63, '13/06/2022');
insert into saque (id, conta_id, valor, data_realizacao) values (6, 8, 1193, '10/05/2022');
insert into saque (id, conta_id, valor, data_realizacao) values (7, 11, 2476, '25/10/2022');
insert into saque (id, conta_id, valor, data_realizacao) values (8, 8, 2331, '09/08/2022');
insert into saque (id, conta_id, valor, data_realizacao) values (9, 1, 1324, '27/02/2023');
insert into saque (id, conta_id, valor, data_realizacao) values (10, 5, 1267, '27/02/2023');

insert into transacao (deposito_id, saque_id, taxa_id, data_realizacao) values (2, 2, 3, '11/06/2022');
insert into transacao (deposito_id, saque_id, taxa_id, data_realizacao) values (4, 1, 2, '25/03/2022');
insert into transacao (deposito_id, saque_id, taxa_id, data_realizacao) values (4, 2, 1, '04/07/2021');
insert into transacao (deposito_id, saque_id, taxa_id, data_realizacao) values (1, 3, 2, '25/10/2021');
insert into transacao (deposito_id, saque_id, taxa_id, data_realizacao) values (5, 5, 5, '17/02/2022');

insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (1, 8, 6, '30/12/2021');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (2, 9, 2, '06/12/2021');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (3, 10, 6, '20/11/2021');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (4, 2, 9, '08/09/2022');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (5, 14, 2, '06/07/2022');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (6, 3, 10, '02/03/2022');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (7, 7, 6, '04/07/2022');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (8, 5, 9, '27/06/2022');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (9, 2, 3, '07/02/2022');
insert into conta_acao (id, conta_id, acao_id, data_realizacao) values (10, 11, 4, '01/01/2022');