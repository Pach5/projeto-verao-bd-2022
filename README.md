# Projeto Verão BD 2022

## Integrantes

- [Giovani Pacheco Signori](https://github.com/GioPach)
- [Guilherme Viana dos Santos](https://github.com/gatoimorrivel)

---

## Introdução

    Surgida da síntese de conhecimentos adquiridos durante o terceiro ano do técnico em desenvolvimento de sistemas, procede-se à ideia de implementar um sistema bancário com o objetivo de expandir a visão interdisciplinar dos alunos. Para tal, promove-se a aproximação de software de empresas estabelecidas como o Banco do Brasil, Nubank e Caixa e o desafio, aos desenvolvedores, de conceder a possibilidade de compra e venda de ações empresariais.
    Em última instância, o presente projeto visa criar um sistema de gerenciamento bancário capaz de viabilizar atividades básicas como saques e depósitos. Ademais, operações como empréstimos, linhas de desconto, crédito e transferências serão funcionalidades priorizadas.
    Entre as tecnologias escolhidas para a implementação, destaca-se o editor de entidade-relacionamento Vuerd, disponível como plugin do editor de texto VSCode, através do qual criarão o esquema do banco de dados. Finalmente, o SGBD (Sistema Gerenciador de Banco de Dados) será o Oracle.

## Usuários

    O perfil dos usuários são variados, visto que um sistema bancário serve a indivíduos de diferentes contextos; porém, as suas necessidades são muito similares. Os clientes precisam de um sistema rápido e confiável que os possibilitem realizar suas tarefas de maneira simples e concisa. Funcionalidades básicas como transações bancárias e visualizar o extrato bancário serão garantidas, assim como compra e venda de ações para usuários com interesse no mercado especulativo.

## Requisitos

    Pretende-se a criação de tabelas para contas bancárias, saques, depósitos, ações, empresas, outrossim, a instituição de seus relacionamentos como uma tabela de transação, associando um saque, um depósito e duas contas bancárias para manter um histórico claro das atividades de cada usuário. Constará a possibilidade de consultas a views dos extratos de cada cada cliente, empresas com maior número de acionistas e capital, além de rastreamento de usuários com alto rendimento para propiciar promoções direcionadas.
