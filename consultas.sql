/* 1.
    * Consulta para relacionar empresas e corretoras associadas
*/
select distinct e.nome as empresa, c.nome as corretora_associada 
from empresa e 
inner join acao a on e.id = a.empresa_id
inner join corretora c on c.id = a.corretora_id
order by e.nome;

/* 2.
    * Consulta para relacionar número do banco de uma conta,
    * o nome e cpf do seu portador, bem como seu tipo.
*/
select c.numero_banco, cl.nome as cliente, cl.cpf as cpf_cliente, t.tipo
from conta c 
inner join conta_cliente cc on c.id = cc.conta_id
inner join cliente cl on cl.id = cc.cliente_id
inner join tipo_cliente t on t.id = cc.tipo_cliente_id
order by numero_banco;

/* 3.
    * Consulta para relacionar nome e cpf do cliente
    * com o nome e cnpj da sua empresa cadastrada no banco.
*/
select cl.nome as cliente, cl.cpf, e.nome as empresa, e.cnpj from cliente cl
inner join empresa_cliente ec on cl.id = ec.cliente_id
inner join empresa e on e.id = ec.empresa_id
order by cliente;

/* 4.
    * Consulta para relacionar contas com a soma dos 
    * depósitos e saques por elas realizados.
*/
select c.id as conta, 'R$ '||to_char(sum(d.valor), 'FM999G999G999D90') as Soma_Depositos, 'R$ '||to_char(sum(s.valor), 'FM999G999G999D90') as Soma_Saques
from conta c 
inner join deposito d on c.id = d.conta_id
inner join saque s on c.id = s.conta_id
group by c.id
order by conta;

/* 5.
    * Consulta para relacionar contas com os seguros adquiridos,
    * bem como as datas em que foram realizados e de sua validade.
*/
select c.id as conta, s.tipo as seguro, sc.data_cadastro as data_realizacao, sc.data_validade
from conta c
inner join seguro_conta sc on c.id = sc.conta_id
inner join seguro s on s.id = sc.seguro_id
order by conta;

/* 6.
    * Consulta para obter o nome de um cliente, o número do seu 
    * cartão de crédito, bem como a respectiva bandeira.
*/
select cl.nome as cliente, ct.numero as numero_cartao, b.nome as bandeira 
from cliente cl
inner join conta_cliente cc on cc.cliente_id = cl.id
inner join conta c on cc.conta_id = c.id
inner join cartao ct on c.id = ct.conta_id
inner join bandeira b on ct.bandeira_id = b.id
order by cliente;

/* 7.
    * Consulta para relacionar uma conta aos empréstimos por ela 
    * realizados, bem como o total de parcelas pagas até o momento.
*/
select c.id as conta, 'R$ '||to_char(e.valor_total, 'FM999G999G999D90') as emprestimo, 'R$ '||to_char(sum(p.valor), 'FM999G999G999D90') as total_atual_pago
from conta c
inner join emprestimo e on c.id = e.conta_id
inner join parcela p on e.id = p.emprestimo_id
group by c.id, e.valor_total
order by conta;

/* 8.
    * Consulta para relacionar uma conta ao deposito e saque realizados
    * em uma transferência, bem como à taxa aplicada.
*/
select c.id as conta, t.deposito_id as deposito, t.saque_id as saque, tx.taxa
from conta c 
inner join deposito d on d.conta_id = c.id
inner join transacao t on t.deposito_id = d.id
inner join taxa tx on tx.id = t.taxa_id
order by conta;

/* 9.
    * Consulta para relacionar uma corretora as suas ações,
    * bem como ao valor base e tipo destas.
*/
select c.nome as corretora, a.nome as acao, 'R$ '||to_char(a.valor_base, 'FM999G999G999D90') as valor_base, ta.tipo as tipo_acao
from corretora c
inner join acao a on c.id = a.corretora_id
inner join tipo_acao ta on ta.id = a.tipo_acao_id
order by corretora;

/* 10.
    * Consulta para relacionar os dados de uma empresa
    * ao seu rendimento em janeiro
*/
select e.nome as empresa, e.cnpj, e.email, round(r.percentual, 2) as rendimento_janeiro
from empresa e 
inner join rendimento r on r.empresa_id = e.id
where mes like 'Janeiro/2022'
order by empresa;

/* 11.
    * Consulta para relacionar as ações e a 
    * conta que as possui
*/
select conta.id as conta_id, acao.nome as acao_nome,acao.valor_base as acao_valor from conta_acao
inner join acao on conta_acao.id = acao.id  
inner join conta on conta_acao.conta_id = conta.id
order by conta.id;

/* 12.
    * Consulta para relacionar uma conta
    * a mensalidade total de seus seguros
*/
select conta.id as conta_id, sum(seguro.mensalidade) as mensalidade_total from seguro_conta
inner join seguro on seguro.id = seguro_conta.seguro_id
inner join conta on conta.id = seguro_conta.conta_id
group by conta.id
order by conta.id;

/* 13.
    * Consulta para listar as formas de contato 
    * de um cliente
*/
select cliente.id, cliente.nome as nome, cliente.email as email, 
cliente.telefone as fone 
from cliente;

/* 14.
    * Consulta para listar todos cartões VISA
*/
select * from cartao
inner join bandeira on cartao.bandeira_id = bandeira.id
where bandeira.nome = 'Visa';

/* 15.
    * Consulta para listar quantidade de cartões
    * por bandeira
*/
select bandeira.nome as bandeira, count(cartao.id) as quantidade from cartao
inner join bandeira on cartao.bandeira_id = bandeira.id
group by bandeira.nome;

/* 16.
    * Consulta para listar a quantidade de contas por tipo
*/
select tipo_conta.tipo as tipo, count(conta.id) as quantidade from tipo_conta
inner join conta on conta.tipo_conta_id = tipo_conta.id
group by tipo_conta.tipo;

/* 17.
    * Consulta para mostrar media de rendimento 
    * das empresas
*/
select empresa.nome as nome, avg(rendimento.percentual) as percentual_total from empresa 
inner join rendimento on rendimento.empresa_id = empresa.id
group by empresa.nome;

/* 18.
    * Consulta para mostrar nomes de um cliente e sua empresa
*/
select cliente.nome, empresa.nome from empresa_cliente
inner join cliente on cliente.id = empresa_cliente.cliente_id
inner join empresa on empresa.id = empresa_cliente.empresa_id;

/* 20.
    * Mostrar quantidade de cartões por conta
*/
select conta.id, count(cartao.id) as quantidade from cartao
inner join conta  on conta.id = cartao.conta_id
group by conta.id
order by conta.id

/* 20.
    * Mostrar soma e quantidade de ações de uma conta
*/
select conta.id as conta_id, count(acao.id) as quantidade_acoes, sum(acao.valor_base) as valor_total from acao
inner join conta_acao on acao.id = conta_acao.acao_id
inner join conta on conta.id = conta_acao.conta_id
group by conta.id
order by conta.id

/*
TODO: Deverá ter no mínimo 20 consultas ao todo.
TODO: Das consultas, no mínimo 14 consultas apresentando joins e/ou subconsultas.
*/
