
CREATE TABLE acao
(
  id           NUMBER   NOT NULL,
  nome         VARCHAR2 NOT NULL,
  empresa_id   NUMBER   NOT NULL,
  corretora_id NUMBER   NOT NULL,
  tipo_acao_id NUMBER   NOT NULL,
  valor_base   NUMBER   NOT NULL,
  CONSTRAINT PK_acao PRIMARY KEY (id)
);

CREATE TABLE bandeira
(
  id   NUMBER   NOT NULL,
  nome VARCHAR2 NOT NULL,
  CONSTRAINT PK_bandeira PRIMARY KEY (id)
);

CREATE TABLE cartao
(
  id            NUMBER   NOT NULL,
  conta_id      NUMBER   NOT NULL,
  bandeira_id   NUMBER   NOT NULL,
  numero        VARCHAR2 NOT NULL,
  cvv           VARCHAR2 NOT NULL,
  data_validade DATE     NOT NULL,
  CONSTRAINT PK_cartao PRIMARY KEY (id)
);

CREATE TABLE cliente
(
  id              NUMBER   NOT NULL,
  nome            VARCHAR2 NOT NULL,
  cpf             VARCHAR2 NOT NULL,
  email           VARCHAR2 NOT NULL,
  telefone        VARCHAR2,
  data_nascimento DATE     NOT NULL,
  data_cadastro   DATE     DEFAULT CURRENT_DATE NOT NULL,
  CONSTRAINT PK_cliente PRIMARY KEY (id)
);

CREATE TABLE conta
(
  id            NUMBER   NOT NULL,
  numero_banco  VARCHAR2 NOT NULL,
  limite        NUMBER   NOT NULL,
  saldo         NUMBER   NOT NULL,
  senha         VARCHAR2 NOT NULL,
  salt          VARCHAR2 NOT NULL,
  data_cadastro DATE     DEFAULT CURRENT_DATE NOT NULL,
  tipo_conta_id NUMBER   NOT NULL,
  CONSTRAINT PK_conta PRIMARY KEY (id)
);

CREATE TABLE conta_acao
(
  id              NUMBER NOT NULL,
  conta_id        NUMBER,
  acao_id         NUMBER,
  data_realizacao DATE  ,
  CONSTRAINT PK_conta_acao PRIMARY KEY (id)
);

CREATE TABLE conta_cliente
(
  cliente_id      NUMBER NOT NULL,
  conta_id        NUMBER NOT NULL,
  tipo_cliente_id NUMBER NOT NULL,
  CONSTRAINT PK_conta_cliente PRIMARY KEY (cliente_id, conta_id)
);

CREATE TABLE corretora
(
  id            NUMBER   NOT NULL,
  nome          VARCHAR2 NOT NULL,
  descricao     VARCHAR2 NOT NULL,
  cnpj          VARCHAR2 NOT NULL,
  telefone      VARCHAR2,
  email         VARCHAR2 NOT NULL,
  data_cadastro DATE     DEFAULT CURRENT_DATE NOT NULL,
  CONSTRAINT PK_corretora PRIMARY KEY (id)
);

CREATE TABLE deposito
(
  id              NUMBER NOT NULL,
  conta_id        NUMBER NOT NULL,
  valor           NUMBER NOT NULL,
  data_realizacao DATE   NOT NULL,
  CONSTRAINT PK_deposito PRIMARY KEY (id)
);

CREATE TABLE empresa
(
  id            NUMBER   NOT NULL,
  nome          VARCHAR2 NOT NULL,
  descricao     VARCHAR2 NOT NULL,
  cnpj          VARCHAR2 NOT NULL,
  data_cadastro DATE     DEFAULT CURRENT_DATE NOT NULL,
  telefone      VARCHAR2,
  email         VARCHAR2 NOT NULL,
  CONSTRAINT PK_empresa PRIMARY KEY (id)
);

CREATE TABLE empresa_cliente
(
  cliente_id      NUMBER   NOT NULL,
  empresa_id      NUMBER   NOT NULL,
  data_assinatura DATE     NOT NULL,
  data_validade   DATE     NOT NULL,
  contrato        VARCHAR2 NOT NULL,
  CONSTRAINT PK_empresa_cliente PRIMARY KEY (cliente_id, empresa_id)
);

CREATE TABLE emprestimo
(
  id              NUMBER NOT NULL,
  conta_id        NUMBER,
  valor_total     NUMBER NOT NULL,
  juros           NUMBER NOT NULL,
  data_realizacao DATE   NOT NULL,
  CONSTRAINT PK_emprestimo PRIMARY KEY (id)
);

CREATE TABLE parcela
(
  id             NUMBER NOT NULL,
  emprestimo_id  NUMBER NOT NULL,
  valor          NUMBER NOT NULL,
  data_pagamento DATE  ,
  data_agendada  DATE   NOT NULL,
  CONSTRAINT PK_parcela PRIMARY KEY (id)
);

CREATE TABLE rendimento
(
  id         NUMBER   NOT NULL,
  mes        VARCHAR2 NOT NULL,
  percentual NUMBER   NOT NULL,
  empresa_id NUMBER  ,
  CONSTRAINT PK_rendimento PRIMARY KEY (id)
);

CREATE TABLE saque
(
  id              NUMBER NOT NULL,
  conta_id        NUMBER NOT NULL,
  valor           NUMBER NOT NULL,
  data_realizacao DATE   NOT NULL,
  CONSTRAINT PK_saque PRIMARY KEY (id)
);

CREATE TABLE seguro
(
  id          NUMBER   NOT NULL,
  tipo        VARCHAR2 NOT NULL,
  descricao   VARCHAR2 NOT NULL,
  mensalidade NUMBER   NOT NULL,
  indenizacao NUMBER  ,
  CONSTRAINT PK_seguro PRIMARY KEY (id)
);

CREATE TABLE seguro_conta
(
  conta_id      NUMBER NOT NULL,
  seguro_id     NUMBER NOT NULL,
  data_cadastro DATE   DEFAULT CURRENT_DATE NOT NULL,
  data_validade DATE   NOT NULL,
  CONSTRAINT PK_seguro_conta PRIMARY KEY (conta_id, seguro_id)
);

CREATE TABLE taxa
(
  id   NUMBER   NOT NULL,
  tipo VARCHAR2 NOT NULL,
  taxa NUMBER   NOT NULL,
  CONSTRAINT PK_taxa PRIMARY KEY (id)
);

CREATE TABLE tipo_acao
(
  id        NUMBER   NOT NULL,
  tipo      VARCHAR2 NOT NULL,
  descricao VARCHAR2 NOT NULL,
  CONSTRAINT PK_tipo_acao PRIMARY KEY (id)
);

CREATE TABLE tipo_cliente
(
  id   NUMBER   NOT NULL,
  tipo VARCHAR2 NOT NULL,
  CONSTRAINT PK_tipo_cliente PRIMARY KEY (id)
);

CREATE TABLE tipo_conta
(
  id   NUMBER   NOT NULL,
  tipo VARCHAR2 NOT NULL,
  CONSTRAINT PK_tipo_conta PRIMARY KEY (id)
);

CREATE TABLE transacao
(
  deposito_id     NUMBER NOT NULL,
  saque_id        NUMBER NOT NULL,
  taxa_id         NUMBER NOT NULL,
  data_realizacao DATE   DEFAULT CURRENT_DATE,
  CONSTRAINT PK_transacao PRIMARY KEY (deposito_id, saque_id)
);

ALTER TABLE conta_cliente
  ADD CONSTRAINT FK_conta_TO_conta_cliente
    FOREIGN KEY (conta_id)
    REFERENCES conta (id);

ALTER TABLE cartao
  ADD CONSTRAINT FK_bandeira_TO_cartao
    FOREIGN KEY (bandeira_id)
    REFERENCES bandeira (id);

ALTER TABLE conta_cliente
  ADD CONSTRAINT FK_cliente_TO_conta_cliente
    FOREIGN KEY (cliente_id)
    REFERENCES cliente (id);

ALTER TABLE saque
  ADD CONSTRAINT FK_conta_TO_saque
    FOREIGN KEY (conta_id)
    REFERENCES conta (id);

ALTER TABLE deposito
  ADD CONSTRAINT FK_conta_TO_deposito
    FOREIGN KEY (conta_id)
    REFERENCES conta (id);

ALTER TABLE transacao
  ADD CONSTRAINT FK_deposito_TO_transacao
    FOREIGN KEY (deposito_id)
    REFERENCES deposito (id);

ALTER TABLE transacao
  ADD CONSTRAINT FK_saque_TO_transacao
    FOREIGN KEY (saque_id)
    REFERENCES saque (id);

ALTER TABLE seguro_conta
  ADD CONSTRAINT FK_conta_TO_seguro_conta
    FOREIGN KEY (conta_id)
    REFERENCES conta (id);

ALTER TABLE seguro_conta
  ADD CONSTRAINT FK_seguro_TO_seguro_conta
    FOREIGN KEY (seguro_id)
    REFERENCES seguro (id);

ALTER TABLE empresa_cliente
  ADD CONSTRAINT FK_cliente_TO_empresa_cliente
    FOREIGN KEY (cliente_id)
    REFERENCES cliente (id);

ALTER TABLE conta
  ADD CONSTRAINT FK_tipo_conta_TO_conta
    FOREIGN KEY (tipo_conta_id)
    REFERENCES tipo_conta (id);

ALTER TABLE cartao
  ADD CONSTRAINT FK_conta_TO_cartao
    FOREIGN KEY (conta_id)
    REFERENCES conta (id);

ALTER TABLE conta_cliente
  ADD CONSTRAINT FK_tipo_cliente_TO_conta_cliente
    FOREIGN KEY (tipo_cliente_id)
    REFERENCES tipo_cliente (id);

ALTER TABLE transacao
  ADD CONSTRAINT FK_taxa_TO_transacao
    FOREIGN KEY (taxa_id)
    REFERENCES taxa (id);

ALTER TABLE emprestimo
  ADD CONSTRAINT FK_conta_TO_emprestimo
    FOREIGN KEY (conta_id)
    REFERENCES conta (id);

ALTER TABLE parcela
  ADD CONSTRAINT FK_emprestimo_TO_parcela
    FOREIGN KEY (emprestimo_id)
    REFERENCES emprestimo (id);

ALTER TABLE empresa_cliente
  ADD CONSTRAINT FK_empresa_TO_empresa_cliente
    FOREIGN KEY (empresa_id)
    REFERENCES empresa (id);

ALTER TABLE acao
  ADD CONSTRAINT FK_empresa_TO_acao
    FOREIGN KEY (empresa_id)
    REFERENCES empresa (id);

ALTER TABLE acao
  ADD CONSTRAINT FK_corretora_TO_acao
    FOREIGN KEY (corretora_id)
    REFERENCES corretora (id);

ALTER TABLE rendimento
  ADD CONSTRAINT FK_empresa_TO_rendimento
    FOREIGN KEY (empresa_id)
    REFERENCES empresa (id);

ALTER TABLE acao
  ADD CONSTRAINT FK_tipo_acao_TO_acao
    FOREIGN KEY (tipo_acao_id)
    REFERENCES tipo_acao (id);

ALTER TABLE conta_acao
  ADD CONSTRAINT FK_conta_TO_conta_acao
    FOREIGN KEY (conta_id)
    REFERENCES conta (id);

ALTER TABLE conta_acao
  ADD CONSTRAINT FK_acao_TO_conta_acao
    FOREIGN KEY (acao_id)
    REFERENCES acao (id);
